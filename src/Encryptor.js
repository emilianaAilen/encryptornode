const tar = require('tar-fs')
const crypto = require('crypto')
const fs = require('fs')

export default class Encryptor {
    constructor() {
        this.key = crypto.randomBytes(16) //contraseña
        this.encrypt = crypto.createCipher('aes-256-cbc', this.key) //crea cipher con tipo de cifrado y key
        this.decrypt = crypto.createDecipher('aes-256-cbc', this.key) //crea decipher con tipo de cifrado y key
    }
    Encrypt() {
        tar.pack('./files').pipe(this.encrypt).pipe(fs.createWriteStream('files.tar')) //lee los archivos que estan en la carpeta files y los encripta, crea una nueva carpeta donde estan comprimidos
     }
    Decrypt() {
        fs.createReadStream('files.tar').pipe(this.decrypt).pipe(tar.extract('./decifrados')) //desencripta los archivos y los guarda en una nueva carpeta "decifrados"
    }
}