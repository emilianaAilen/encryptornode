"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var tar = require('tar-fs');

var crypto = require('crypto');

var fs = require('fs');

var Encryptor = /*#__PURE__*/function () {
  function Encryptor() {
    _classCallCheck(this, Encryptor);

    this.key = crypto.randomBytes(16); //contraseña

    this.encrypt = crypto.createCipher('aes-256-cbc', this.key); //crea cipher con tipo de cifrado y key

    this.decrypt = crypto.createDecipher('aes-256-cbc', this.key); //crea decipher con tipo de cifrado y key
  }

  _createClass(Encryptor, [{
    key: "Encrypt",
    value: function Encrypt() {
      tar.pack('./files').pipe(this.encrypt).pipe(fs.createWriteStream('files.tar')); //lee los archivos que estan en la carpeta files y los encripta, crea una nueva carpeta donde estan comprimidos
    }
  }, {
    key: "Decrypt",
    value: function Decrypt() {
      fs.createReadStream('files.tar').pipe(this.decrypt).pipe(tar.extract('./decifrados')); //desencripta los archivos y los guarda en una nueva carpeta "decifrados"
    }
  }]);

  return Encryptor;
}();

exports["default"] = Encryptor;